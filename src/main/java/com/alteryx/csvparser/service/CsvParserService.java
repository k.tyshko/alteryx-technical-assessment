package com.alteryx.csvparser.service;

import com.alteryx.csvparser.dto.CsvColumnDto;
import com.alteryx.csvparser.dto.CsvInfoDto;
import com.alteryx.csvparser.exceprion.ParserException;
import com.opencsv.CSVReader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Service
public class CsvParserService {

	public List<CsvInfoDto> getCsvInfoDtos(String folder) {
		var csvFiles = getCsvFiles(folder);
		return Stream.of(csvFiles).map(this::getCsvInfoDto).collect(toList());
	}

	private File[] getCsvFiles(String folder) {
		var file = new File(folder);
		if (!file.exists())
			throw new ParserException("Path " + folder + " does not exists");

		if (!file.isDirectory())
			throw new ParserException("Folder " + folder + " does not exists");

		return file.listFiles((dir, name) -> name.endsWith(".csv"));
	}

	private CsvInfoDto getCsvInfoDto(File file) {
		var columns = new ArrayList<CsvColumnDto>();
		try (CSVReader csvReader = new CSVReader(new FileReader(file))) {
			Optional.ofNullable(csvReader.readNext())
					.stream()
					.flatMap(Stream::of)
				    .map(CsvColumnDto::new)
				    .forEach(columns::add);

			String[] line;
			while ((line = csvReader.readNext()) != null) {
				for (var i = 0; i < line.length; i++) {
					if (line[i] == null || line[i].isBlank()) {
						Optional.ofNullable(columns.get(i)).ifPresent(CsvColumnDto::increaseMissingRows);
					}
				}
			}

			return new CsvInfoDto(file.getName(), csvReader.getLinesRead() - 1, columns);
		}
		catch (IOException  e) {
			throw new ParserException("Failed to parse file " + file.getName(), e);
		}
	}

}
