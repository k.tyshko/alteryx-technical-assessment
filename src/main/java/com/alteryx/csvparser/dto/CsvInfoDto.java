package com.alteryx.csvparser.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class CsvInfoDto {

	private String fileName;
	private Long rowCount;
	private List<CsvColumnDto> columns;
}
