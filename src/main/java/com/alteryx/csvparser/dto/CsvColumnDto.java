package com.alteryx.csvparser.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CsvColumnDto {
	private String name;
	private Long missingRowCount = 0L;

	public CsvColumnDto(String name) {
		this.name = name;
	}

	public void increaseMissingRows() {
		missingRowCount++;
	}
}
