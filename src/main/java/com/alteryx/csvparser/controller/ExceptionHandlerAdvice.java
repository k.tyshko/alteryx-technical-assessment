package com.alteryx.csvparser.controller;

import com.alteryx.csvparser.dto.ErrorResponseDto;
import com.alteryx.csvparser.exceprion.ParserException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler(ParserException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorResponseDto processValidationError(ParserException ex) {
		return new ErrorResponseDto(ex.getMessage());
	}
}
