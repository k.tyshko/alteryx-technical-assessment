package com.alteryx.csvparser.controller;

import com.alteryx.csvparser.service.CsvParserService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class CsvInfoController {

	private final CsvParserService csvParserService;

	@GetMapping("/csv-info")
	public String getCsvInfo(@RequestParam(value = "folderPath") @NonNull String folderPath,  Model model) {
		var csvInfoDtos = csvParserService.getCsvInfoDtos(folderPath);
		model.addAttribute("csvInfoDtos", csvInfoDtos);
		return "csv-info";

	}

}
